#  Dotfiles

![Screenshot of my desktop](https://gitlab.com/BadBrush/dots/-/raw/master/scrot.png)
Dotfiles are the customization files that are used to personalize your Linux or other Unix-based system.  You can tell that a file is a dotfile because the name of the file will begin with a period--a dot!  The period at the beginning of a filename or directory name indicates that it is a hidden file or directory.  This repository contains my personal dotfiles.  They are stored here for convenience so that I may quickly access them on new machines or new installs.  Also, others may find some of my configurations helpful in customizing their own dotfiles.

# Who Am I?

My name is Tim Ridley, I am a professional artist ( https://www.timothyridley.com ) and computer tinkerer. I stole this template from Derek over at DistroTube because I'm a lazy bastard and he has a nicely organized Dotfile directory. As recompense here is a link to his DistroTube channel ( https://www.youtube.com/c/DistroTube ).

# How To Manage Your Own Dotfiles

There are a hundred ways to manage your dotfiles.  My first suggestion would be to read up on the subject.  A great place to start is "Your unofficial guide to dotfiles on GitHub": https://dotfiles.github.io/

Personally, I use the git bare repository method for managing my dotfiles: https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/

# License

The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software.  In other words, do what you want with it.  The  only requirement with the MIT License is that the license and copyright notice must be provided with the software.
